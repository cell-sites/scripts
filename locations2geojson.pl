#!/usr/bin/env perl
#
# Cell Sites Scripts
# Copyright (C) 2017 James Pole
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;
use Readonly;

our $VERSION = 1.0;

Readonly::Scalar my $COLUMN_ID        => 0;
Readonly::Scalar my $COLUMN_NAME      => 1;
Readonly::Scalar my $COLUMN_LATITUDE  => 2;
Readonly::Scalar my $COLUMN_LONGITUDE => 3;

printf "{\n";
printf "\t\"type\": \"FeatureCollection\",\n";
printf "\t\"features\": [\n";

my $started = 0;

while (<>) {

    chomp;

    my @values = split /,/msx;

    if ( !exists $values[$COLUMN_NAME] ) {

        next;

    }

    if ( $started != 0 ) {

        printf ",\n";

    }
    else {

        $started = 1;

    }

    printf "\t\t{\n";
    printf "\t\t\t\"type\": \"Feature\",\n";
    printf "\t\t\t\"id\": \"%d\",\n", $values[$COLUMN_ID];
    printf "\t\t\t\"geometry\": {\n";
    printf "\t\t\t\t\"type\": \"Point\",\n";
    printf "\t\t\t\t\"coordinates\": [%f, %f]\n", $values[$COLUMN_LONGITUDE],
      $values[$COLUMN_LATITUDE];
    printf "\t\t\t},\n";
    printf "\t\t\t\"properties\": {\n";
    printf "\t\t\t\t\"name\": \"%s\"\n", $values[$COLUMN_NAME];
    printf "\t\t\t}\n";
    printf "\t\t}";

}

printf "\n\t]\n";
printf "}\n";
